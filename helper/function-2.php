<?php
function factorialStr($v)
{
    $str = "";
    for ($i = 0; $i <= $v; $i++) {
        $num = abs($i - $v);
        if ($num > 0) {
            if ($num == 1) {
                $str .= abs($num) . '';
            } else {
                $str .= abs($num) . 'x';
            }
        }

    }
    return $str;
}
